
<?php

Breadcrumbs::for('トップ', function ($trail) {
	$trail->add('トップ', route('トップ'));
});
Breadcrumbs::for('受講状況一覧', function ($trail) {
	$trail->parent('トップ');
	$trail->add('受講状況一覧', route('受講状況一覧'));
});
Breadcrumbs::for('未受講者メール編集', function ($trail) {
	$trail->parent('受講状況一覧');
	$trail->add('未受講者メール編集', route('未受講者メール編集'));
});
Breadcrumbs::for('未受講者メール確認', function ($trail) {
	$trail->parent('受講状況一覧');
	$trail->add('未受講者メール確認', route('未受講者メール確認'));
});
Breadcrumbs::for('未受講者メール送信完了', function ($trail) {
	$trail->parent('受講状況一覧');
	$trail->add('未受講者メール送信完了', route('未受講者メール送信完了'));
});



/*

// 例

Breadcrumbs::for('admin.pages.index', function ($trail) {
	$trail->add('Admin', route('admin.pages.index'));
});

Breadcrumbs::for('admin.users.index', function ($trail) {
	$trail->parent('admin.pages.index');
	$trail->add('Users', route('admin.users.index'));
});

Breadcrumbs::for('admin.users.show', function ($trail, User $user) {
	$trail->parent('admin.users.index');
	$trail->add($user->full_name, route('admin.users.show', $user));
});

Breadcrumbs::for('admin.users.edit', function ($trail, User $user) {
	$trail->parent('admin.users.show', $user);
	$trail->add('Edit', route('admin.users.edit', $user));
});

Breadcrumbs::for('admin.users.roles.index', function ($trail, User $user) {
	$trail->parent('admin.users.show', $user);
	$trail->add('Roles', route('admin.users.roles.index', $user));
});

Breadcrumbs::for('admin.users.roles.show', function ($trail, User $user, Role $role) {
	$trail->parent('admin.users.roles.index', $user, $role);
	$trail->add('Edit', route('admin.users.roles.show', [$user, $role]));
});

Breadcrumbs::for('PagesController@getIndex', function ($trail) {
    $trail->add('Home', action('PagesController@getIndex'));
});

Breadcrumbs::for('secret.page', function ($trail) {
    $trail->add('Secret page', url('secret'))
});
*/