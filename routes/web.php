<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\AttendanceController;


Route::get('/', function () {
    return view('welcome');
});

/*P7、P12 Eラーニングトップページ*/
Route::get('top', function () {
    $weekday = array( "日", "月", "火", "水", "木", "金", "土" );
    $data=[];
    $data["period"] = date("n")."月".date("j")."日（".$weekday[date("w", strtotime("10 day"))]."） ～ ".date("n", strtotime("10 day"))."月".date("j", strtotime("10 day"))."日（".$weekday[date("w", strtotime("10 day"))]."）";
    return View::make('top')->with('data',$data);
})->name('トップ');

/*P15 リードページ*/
Route::get('read', function () {
    $data=[];
    $data["period"] = date("Y")."年".date("n")."月 分";
    return View::make('read')->with('data',$data);
})->name('リード');

/*P17 受講ページ*/
Route::get('question', function () {
    $data=[];
    $data["period"] = date("Y")."年".date("n")."月 分";
    return View::make('question')->with('data',$data);
})->name('受講');

/*P21 コメントページ*/
Route::get('comment', function () {
    $data=[];
    $data["period"] = date("Y")."年".date("n")."月 分";
    return View::make('comment')->with('data',$data);
})->name('コメント');

/*P23 受講完了ページ*/
Route::get('complete', function () {
    $data=[];
    $data["period"] = date("Y")."年".date("n")."月 分";
    return View::make('complete')->with('data',$data);
})->name('受講完了');


/*P50 ID/パスワード印刷レイアウトページ*/
Route::get('printout', function () {
	$data=[];
	$str = "山田 太郎様\nコンプライアンスEラーニングの受講をお願いいたします。";
	$data["contents"] = nl2br($str, false);
	return View::make('printout')->with('data',$data);
});

/*P81 教材一覧ページ*/
Route::get('kyozaiichiran', function () {
	return view('kyozaiichiran');
});

/*P81 教材一覧ページ*/
Route::get('kyozaishinkisakusei', function () {
	return view('kyozaishinkisakusei');
});

/*P93 問題編集ページ*/
Route::get('mondaihenshu', function(){
	$data=[];
	$data["mondaino"] = 2;
	$data["title"]='テストタイトル';
	$data["honbun"]='<p><strong>本文テスト</strong>honbun</p>';
	$data["mondaibun"]='どれが正解でしょう';
	$data["keishiki"]='fukusuu';
	$data["kaitou1"]='１つ目の選択肢';
	$data["kaitou1_seikai"]='正解';
	$data["kaitou2"]='２つ目の選択肢';
	$data["kaitou2_seikai"]='';
	$data["kaitou3"]='３つ目の選択肢';
	$data["kaitou3_seikai"]='';
	$data["kaitou4"]='４つ目の選択肢';
	$data["kaitou4_seikai"]='';
	$data["kaitou5"]='５つ目の選択肢';
	$data["kaitou5_seikai"]='';
	$data["kaisetsu"]='とにかく答えは１です。';

	return View::make('mondaihenshu')->with('data', $data);
})->name('問題編集');
Route::post('mondaihenshukakunin', 'MondaihenshukakuninController@postIndex');


/*P25 受講状況一覧ページ*/
Route::resource('attendance', 'AttendanceController');
Route::get('attendance', 'AttendanceController@index')->name('受講状況一覧');
Route::get('attendance/mail/non-attend-edit', function () {
	return view('attendance.mail-non-attend.edit');
})->name('未受講者メール編集');
Route::get('attendance/mail-non-attend-confirm', function () {
	return view('attendance.mail-non-attend.confirm');
})->name('未受講者メール確認');
Route::get('attendance/mail-non-attend-complete', function () {
	return view('attendance.mail-non-attend.complete');
})->name('未受講者メール送');