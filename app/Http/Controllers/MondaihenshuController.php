<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MondaihenshuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=[];
        $data["mondaino"] = 2;
        $data["keishiki"]='fukusuu';
        $data["kaitou1"]='正解';
        return View::make('mondaihenshu')->with('data',$data);
    }
}
