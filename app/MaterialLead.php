<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialLead extends Model
{
	protected $table = 'Material_Lead';
	protected $primaryKey = 'serial_id';
	const CREATED_AT = 'insert_time';
	const UPDATED_AT = 'update_time';
}
