@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="{{ url('/top') }}">トップ</a></li>
        <li><a href="#">教材一覧</a></li>
        <li><a href="#">教材編集</a></li>
        <li class="active">リード編集確認</li>
    </ol>
    <h3>　問題編集　第{{$data["mondaino"]}}問目</h3>
</div>
<form class="form-horizontal col-md-12">
    <!-- {{Form::open(array('url'=>'mondaihenshukakunin', 'method'=>'post'))}} -->
    <div class="form-group">
        <label class="col-md-1 control-label"><span class="textOverflow">タイトル</span></label>
        <div class="col-md-6">
            <input type="text" class="form-control" id="title">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-md-1 control-label">本文　　</label>
        <div class="col-xs-8 col-md-11">
            <textarea id="honbun" class="ckeditor" rows="2"></textarea>
            <!-- <textarea class="ckeditor" id="honbun" name ="honbun" rows="2"></textarea> -->
            <!-- <script>
                CKEDITOR.replace('honbun', {});
                    function test() {
                    var x = CKEDITOR.instances.content.GetData();
                }
            </script> -->
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-1 control-label">問題文　</label>
        <div class="col-md-11">
            <textarea class="form-control" rows="2"  id="mondaibun"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="keishiki" class="col-md-1 control-label">形式　　</label>
        <div class="col-md-2 custom2">
            <!-- {{Form::select('keishiki',[
                'tanitsu' => '単一選択',
                'fukusuu' => '複数選択'], $data["keishiki"]
            )}} -->
            <select class="form-control" id="keishiki">
                <option value="tanitsu">単一選択</option>
                <option value="fukusuu">複数選択</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="kaitou" class="col-md-1 control-label">1.</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kaitou1">
        </div>
        <div class="col-md-1 custom1">
            <select class="form-control" id="kotae1">
                <option value="fuseikai"></option>
                <option value="seikai" selected>正解</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="kaitou" class="col-md-1 control-label">2.</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kaitou2">
        </div>
        <div class="col-md-1 custom1">
            <select class="form-control" id="kotae2">
                <option value="fuseikai"></option>
                <option value="seikai">正解</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="kaitou" class="col-md-1 control-label">解答　3.</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kaitou3">
        </div>
        <div class="col-md-1 custom1">
            <select class="form-control" id="kotae3">
                <option value="fuseikai"></option>
                <option value="seikai">正解</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="kaitou" class="col-md-1 control-label">4.</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kaitou4">
        </div>
        <div class="col-md-1 custom1">
            <select class="form-control" id="kotae4">
                <option value="fuseikai"></option>
                <option value="seikai">正解</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="kaitou" class="col-md-1 control-label">5.</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kaitou5">
        </div>
        <div class="col-md-1 custom1">
            <select class="form-control" id="kotae5">
                <option value="fuseikai"></option>
                <option value="seikai">正解</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="kaisetsu" class="col-md-1 control-label">解説　　</label>
        <div class="col-md-8">
            <input type="text" class="form-control" id="kaisetsu">
        </div>
    </div>
    <!-- <p class="col-md-1"></p> -->
    <button type="button" class="btn col-md-1 mx-5" id="kakunin">　確認　</button>   
    <p class="col-md-1"></p>
    <button type="button" class="btn col-md-1 mx-5" id="modoru">　戻る　</button>   

    <!-- {{Form::submit('確認')}} -->
    <!-- {{Form::close()}}        -->

</form>
@endsection
