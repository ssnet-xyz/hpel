<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>コンプライアンス Eラーニング</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!--hpel用CSS-->
    <link href="{{ asset('css/hpel.css') }}" rel="stylesheet">


</head>
<body>
    <header>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/top') }}">
                        コンプライアンス Eラーニング
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <!--mockup list-->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">モック画面 <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('トップ') }}">トップ</a></li>
                                <li><a href="{{ route('問題編集') }}">問題編集ページ</a></li>
                                <li><a href="{{ route('リード') }}">リードページ</a></li>
                                <li><a href="{{ route('受講') }}">受講ページ</a></li>
                                <li><a href="{{ route('コメント') }}">コメントページ</a></li>
                                <li><a href="{{ route('受講完了') }}">受講完了ページ</a></li>
                                <li><a href="{{ url('/printedit') }}">ID/パスワード印刷編集ページ</a></li>
                                <li><a href="{{ url('/kyozaiichiran') }}">教材一覧ページ</a></li>
                                <ul class="dropdown-submenu" role="menu">
                                    <li><a href="{{ url('/kyozaishinkisakusei') }}">教材新規作成ページ</a></li>
                                    <li><a href="{{ url('/mondaihenshu') }}">問題編集ページ > 問題編集確認ページ</a></li>
                                </ul>
                                <li><a href="{{ route('受講状況一覧') }}">受講状況一覧</a></li>
                                <li><a href="{{ route('未受講者メール編集') }}">未受講者メール編集</a></li>
                                <li><a href="{{ route('未受講者メール確認') }}">未受講者メール確認</a></li>
                                <li><a href="{{ route('未受講者メール送信完了') }}">未受講者メール送信完了</a></li>
                                <li><a href="{{ url('#') }}">設定一覧ページ</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('受講状況一覧') }}">受講状況一覧</a></li>
                        <li><a href="#">教材一覧</a></li>
                        <li><a href="#">コメント一覧</a></li>
                        <li><a href="#">受講ログ一覧</a></li>
                        <li><a href="#">設定</a></li>
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <main class="container-fluid">
        {{ Breadcrumbs::render() }}
        @yield('content')
    </main>

    <footer class="footer">
        <div class="container-fluid">
            <div class="col-md-12 hidden-sm hidden-xs text-right">
                お問い合わせ 総務室 コンプライアンス部
            </div>
            <div class="col-md-12">
                <div id="copyRight">&copy; 2017 - HAKUHODO PRODUCT'S All rights reserved.</div>
                <div id="extensionTel" class="hidden-sm hidden-xs">【内線】14-7211</div>
            </div>                
        </div>
    </footer>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>

</body>
</html>
