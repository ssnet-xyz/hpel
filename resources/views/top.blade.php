@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <!--パン屑リスト-->
    <ol class="breadcrumb">
        <li class="active">トップ</li>
    </ol>
        
    <pre>!未受講　今月の受講期間　{{$data["period"]}} </pre>
    <div class="panel panel-default">
        <div class="panel-heading">Eラーニングを受講する</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="textOverflow"><a href="#">3月度　AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</a></div>
                    <div class="textOverflow"><a href="#">2月度　BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB</a></div>
                    <div class="textOverflow"><a href="#">1月度　CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC</a></div>
                    <div class="textOverflow"><a href="#">12月度　DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD</a></div>
                    <div class="textOverflow"><a href="#">11月度　EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE</a></div>
                    <div class="textOverflow"><a href="#">10月度　FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF</a></div>
                    <div class="textOverflow"><a href="#">9月度　GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG</a></div>
                </div>
                <div class="col-md-6">
                    <div class="textOverflow"><a href="#">8月度　HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH</a></div>
                    <div class="textOverflow"><a href="#">7月度臨時　IIIIIIIIIIIIIIIIIIIIIIIIIIIIIII</a></div>
                    <div class="textOverflow"><a href="#">7月度　JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ</a></div>
                    <div class="textOverflow"><a href="#">6月度　KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK</a></div>
                    <div class="textOverflow"><a href="#">5月度　LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL</a></div>
                    <div class="textOverflow"><a href="#">4月度　MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM</a></div>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-bordered">
        <tr class="active"><td>山田 太郎さんの受講状況</td></tr>
        <tr>
            <td>
                <table class="table table-bordered table-condensed text-center tableFixed">
                    <tr class="active"><td>4月分</td><td>5月分</td><td>6月分</td><td>7月分</td><td>8月分</td><td>9月分</td></tr>
                    <tr><td>済</td><td>済</td><td>済</td><td>済</td><td>済</td><td>済</td></tr>
                    <tr class="active"><td>10月分</td><td>11月分</td><td>12月分</td><td>1月分</td><td>2月分</td><td>3月分</td></tr>
                    <tr><td>済</td><td>済</td><td>済</td><td>済</td><td>済</td><td>未</td></tr>
                    <tr class="active"><td>7月分臨時</td><td></td><td></td><td></td><td></td><td></td></tr>
                    <tr><td>未</td><td></td><td></td><td></td><td></td><td></td></tr>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered text-left">
                <tr class="active"><td>担当部署一覧（コンプライアンスリーダー）</td></tr>
                <tr><td>総務・コンプライアンス室 コンプライアンス部</td></tr>
            </table>
            <table class="table table-bordered text-left">
                <tr class="active"><td colspan="2">未受講者一覧</td></tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-default">受講状況を見る（未受講者へのメールはこちらから）</button>
                    </td>
                </tr>
                <tr><td style="width:30%">名前</td><td style="width:70%">部署</td></tr>
                <tr>
                    <td><a href="#">山田 太郎</a></td>
                    <td>総務・コンプライアンス室 コンプライアンス部</td>
                </tr>
                <tr>
                    <td><a href="#">山田 花子</a></td>
                    <td>総務・コンプライアンス室 コンプライアンス部</td>
                </tr>
            </table>
            
        </div>
        <div class="col-md-6">
            <table class="table table-bordered text-left">
                <tr class="active"><td colspan="2">リンク集</td></tr>
                <tr><td style="width:30%">名前</td><td style="width:70%">説明</td></tr>
                <tr>
                    <td><a href="#">コンプライアンス図書館</a></td>
                    <td>コンプラタイムやEラーニングなど、コンプライアンスに関する各種教材・資料が保管されています。</td>
                </tr>
                <tr>
                    <td><a href="#">コンプライアンス図書館</a></td>
                    <td>コンプラタイムやEラーニングなど、コンプライアンスに関する各種教材・資料が保管されています。</td>
                </tr>
            </table>
        </div>
    </div>

</div>
@endsection
