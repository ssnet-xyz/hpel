@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <!--パン屑リスト-->
    <ol class="breadcrumb">
        <li><a href="{{ url('/top') }}">トップ</a></li>
        <li class="active">{{$data["period"]}}</li>
    </ol>
    
    <div class="row">
        <div id="pageTitle" class="col-md-12">
            <h3>教材のタイトルを表示する</h3>
        </div>
    </div>
        
    <div class="row">
        <div id="readMain" class="col-md-12">
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
            教材の本文（リッチテキスト）を表示する<br />
        </div>
    </div>

    <div class="row" style="margin-top: 15px;">
        <div class="col-xs-6 col-sm-3 col-md-1">
            <button id="btnReturn" type="button" class="btn btn-default btn-block" onClick="btnReturn_Click()">戻る</button>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-1">
            <button id="btnNext" type="button" class="btn btn-default btn-block" onClick="btnNext_Click()">次へ</button>
        </div>
    </div>

</div>
@endsection
