@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <!--パン屑リスト-->
    <ol class="breadcrumb">
    <li><a href="{{ url('/top') }}">トップ</a></li>
    <li><a href="#">{{$data["period"]}}</a></li>
    <li class="active">受講完了</li>
</ol>
    
    <div class="row">
        <div id="pageTitle" class="col-md-12">
            <h3>受講完了</h3>
        </div>
    </div>

    <div style="margin-bottom: 100px;">
        <p>今回分の受講が完了しました。お疲れ様でした。</p>
    </div>

    <div class="row" style="margin-top: 15px;">
        <div class="col-xs-6 col-sm-3 col-md-1">
            <button id="btnReturn" type="button" class="btn btn-default btn-block" onClick="btnReturn_Click()">戻る</button>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-1">
            <button id="btnNext" type="button" class="btn btn-default btn-block" onClick="btnNext_Click()">次へ</button>
        </div>
    </div>

</div>
@endsection
