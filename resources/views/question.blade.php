@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <!--パン屑リスト-->
    <ol class="breadcrumb">
        <li><a href="{{ url('/top') }}">トップ</a></li>
        <li><a href="#">{{$data["period"]}}</a></li>
        <li class="active">Q1</li>
    </ol>
    
    <div class="row">
        <div id="pageTitle" class="col-md-12">
            <h3>問題のタイトルを表示する</h3>
        </div>
    </div>
        
    <div class="row">
        <div id="questionLeft" class="col-md-6" style="margin-bottom:15px;">
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
            問題の本文（リッチテキスト）を表示する<br />
        </div>

        <div id="questionRight" class="col-md-6">
            <div id="question">
                <h5>質問文が入ります（選択肢1、4選択で正解）</h5>
            </div>
            <form id="answer">
                <div class="checkbox">
                    <label><input id="cb1" value="" type="checkbox"> 1. <span id="cb1Text">回答選択肢 1 の文章が入ります</span></label>
                </div>
                <div class="checkbox">
                    <label><input id="cb2" value="" type="checkbox"> 2. <span id="cb2Text">回答選択肢 2 の文章が入ります</span></label>
                </div>
                <div class="checkbox">
                    <label><input id="cb3" value="" type="checkbox"> 3. <span id="cb3Text">回答選択肢 3 の文章が入ります</span></label>
                </div>
                <div class="checkbox">
                    <label><input id="cb4" value="" type="checkbox"> 4. <span id="cb4Text">回答選択肢 4 の文章が入ります</span></label>
                </div>
                <div class="checkbox">
                    <label><input id="cb5" value="" type="checkbox"> 5. <span id="cb5Text">回答選択肢 5 の文章が入ります</span></label>
                </div>
                <input id="btnAnswer" type="button" class="btn btn-default" value="回答する" onClick="btnAnswer_Click()">
            </form>
            <div id="result" style="margin-top: 15px;">
                <div id="success" style="display: none;">
                    <p class="text-danger">◎正解です！</p>
                    <div id="explainText">
                        解説文が入ります<br />
                        解説文が入ります<br />
                        解説文が入ります<br />
                    </div>                
                </div>
                <div id="failure" style="display: none;">
                    <p class="text-primary">×残念、違います</p>
                    もう一度回答してください。<br />
                    選択肢はシャッフルされます。<br />
                    <button id="btnReTry" type="button" class="btn btn-default" onClick="btnReTry_Click()">もう一度回答する</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 15px;">
        <div class="col-xs-6 col-sm-3 col-md-1">
            <button id="btnReturn" type="button" class="btn btn-default btn-block" onClick="btnReturn_Click()">戻る</button>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-1">
            <button id="btnNext" type="button" class="btn btn-default btn-block" disabled onClick="btnNext_Click()">次へ</button>
        </div>
    </div>

</div>

<script type="text/javascript">

    var index = [1, 2, 3, 4, 5];

    var cbText = [];
    for (var i = 1; i <= index.length; i++){
        var elem = document.getElementById("cb" + i + "Text");
        cbText.push(elem.innerHTML);
    }

    ansShuffle();

    /*正解の問題番号*/
    var ansIndex = [1, 4];

    /*問題文シャッフル*/
    function ansShuffle() {
        var shuffle = function() {return Math.random()-.5};
        index.sort(shuffle)
        for (var i = 1; i <= index.length; i++){
            document.getElementById("cb" + i + "Text").innerHTML = cbText[index[i - 1] - 1];
        }
    }    

    /*正解確認*/
    function ansCheck() {
        var selectIndex = [];

        for (var i = 1; i <= 5; i++) {
            if (document.getElementById("cb" + i).checked) {
                selectIndex.push(index[i-1]);
            }
        }

        selectIndex.sort(function(a,b) {
            if( a < b ) return -1;
            if( a > b ) return 1;
            return 0;
        });

        var isEqual = false;
        if (ansIndex.toString() == selectIndex.toString()) {
            isEqual = true;
        }

        return isEqual
    }

    /*クリックイベント*/
    function btnAnswer_Click() {
        /*回答欄の非活性化*/
        var input_tags = document.getElementById("answer").getElementsByTagName("input");
        for(var i=0;i<input_tags.length;i++){
            input_tags[i].disabled = true;
        }

        if (ansCheck()) {
            document.getElementById("success").style.display="block";
            document.getElementById("failure").style.display="none";
            document.getElementById("btnNext").disabled=false;
        } else {
            document.getElementById("success").style.display="none";
            document.getElementById("failure").style.display="block";
            document.getElementById("btnNext").disabled=true;            
        }
    }
    function btnReTry_Click() {
        document.getElementById("failure").style.display="none";
        var input_tags = document.getElementById("answer").getElementsByTagName("input");
        for(var i=0;i<input_tags.length;i++) {
            input_tags[i].disabled = false;
            input_tags[i].checked = false;
        }
        ansShuffle();
    }
    function btnReturn_Click() {
    }
    function btnNext_Click() {
    }
</script>
@endsection
