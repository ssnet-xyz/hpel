@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <h2>受講状況一覧</h2>

                <!-- Search-form -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        +　検索条件
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">部門</label>
                                <div class="col-sm-2"><select class="form-control" name=""></select></div>
                                <div class="col-sm-2 col-sm-offset-2"><input class="form-control" name=""></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">部署</label>
                                <div class="col-sm-2"><select class="form-control" name=""></select></div>
                                <div class="col-sm-2 col-sm-offset-2"><input class="form-control" name=""></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">チーム</label>
                                <div class="col-sm-2"><select class="form-control" name=""></select></div>
                                <div class="col-sm-2 col-sm-offset-2"><select class="form-control" name=""></select>
                                </div>
                                <div class="col-sm-2"><select class="form-control" name=""></select></div>
                                <div class="col-sm-2"><select class="form-control" name=""></select></div>
                            </div>
                            <div class="btn-toolbar pull-right">
                                <div class="btn btn-primary">検索</div>
                                <div class="btn btn-default">クリア</div>
                            </div>
                        </form>
                    </div>
                </div>

                <h3>受講者名</h3>
                <div class="panel">
                    <div class="btn btn-default">チェックしたユーザーに受講依頼メールを</div>
                    <a href="{{ route('未受講者メール編集') }}" class="btn btn-default">未受講者のいるリーダーにメールを送る</a>
                    <div class="btn btn-default">Excel出力</div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <td style="vertical-align: bottom;">
                                <input class="form-control" type="checkbox" checked="checked">
                            </td>
                            <th>部門</th>
                            <th>部署</th>
                            <th>チーム</th>
                            <th>コンプライアンス<br>リーダー</th>
                            <th>受講者名</th>
                            <th>4月分</th>
                            <th>5月分</th>
                            <th>6月分</th>
                            <th>7月分</th>
                            <th>7月臨時</th>
                            <th>8月分</th>
                            <th>…</th>
                        </tr>
                        </thead>
                        <tbody>
                        @for ($i = 1; $i < 10; $i++)
                            <tr>
                                <td><input type="checkbox" class="form-control" name="" value=""></td>
                                <td>総務室</td>
                                <td>コンプラ</td>
                                <td>リスマネ</td>
                                <td>山田　太郎</td>
                                <td><a href="#">山田　花子</a></td>
                                <td>対象外</td>
                                <td>対象外</td>
                                <td>合格</td>
                                <td>合格</td>
                                <td>未</td>
                                <td>未</td>
                                <td></td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                    <nav aria-label="...">
                        <ul class="pagination pagination-sm">
                            <li class="disabled"><a href="#" aria-label="Previous"><span
                                            aria-hidden="true">&laquo;</span></a></li>
                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">2 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">3 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">4 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
