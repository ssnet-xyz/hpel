@extends('layouts.app')

@section('content')
    <div class="col-md-12" style="min-height:200px">

        <h2>未受講者メール送信完了</h2>


        <h4>送信が完了しました。</h4>
    </div>

    <div style="margin-top: 15px;">
        <div class="col-xs-2">
            <a href="{{route('受講状況一覧')}}" id="btnConfirm" type="button"
               class="btn btn-default btn-block">受講状況一覧に戻る</a>
        </div>
    </div>

@endsection