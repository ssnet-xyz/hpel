@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <h2>未受講者メール確認</h2>

                <!-- Search-form -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        FROM
                    </div>
                    <div class="panel-body">
                        山田 花子
                    </div>
                </div>

                <!-- Search-form -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        TO
                    </div>
                    <div class="panel-body">
                        {{-- style-タッグ、ごめん！--}}
                        @for ($i = 1; $i < 12; $i++)
                            <div class="col-sm-6" style="margin-bottom:0.5em" onClick="$(this).fadeOut();">
                                山田 花子（総務室 コンプラ リスマネ）
                            </div>
                        @endfor

                    </div>
                </div>


                <h3>件名</h3>
                <input type="text" class="form-control" readonly="readonly" value="コンプライアンスEラーニング受講のお願い">

                <h3>本文</h3>
                <textarea class="form-control" readonly="readonly" name="" id="" cols="30" rows="5">
各位
今月のコンプライアンスEラーニングが未受講になっています。
各自速やかに受講をお願いいたします
                </textarea>

                <div class="row" style="margin-top: 15px;">
                    <div class="col-xs-1">
                        <a href="{{ route('未受講者メール編集') }}" id="btnReturn" type="button" class="btn btn-default btn-block">戻る</a>
                    </div>
                    <div class="col-xs-1">
                        <a href="{{ route('未受講者メール送信完了') }}" id="btnNext" type="button" class="btn btn-default btn-block">送信</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection