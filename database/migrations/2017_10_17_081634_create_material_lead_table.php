<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Material_Lead', function (Blueprint $table) {
            $table->unsignedInteger('serial_id')->unique();
	        $table->smallInteger('year');
	        $table->tinyInteger('month');
			$table->boolean('special')->default(false);
			$table->string('category', 256)->nullable();
			$table->string('title', 256)->nullable();
			$table->text('image')->nullable();
			$table->tinyInteger('status')->default(false);
			$table->boolean('invalid_flag')->default(false);
	        $table->string('create_user', 64)->nullable();
	        $table->dateTime('insert_time')->nullable();
	        $table->dateTime('update_time')->nullable();
	        $table->primary(['year', 'month', 'special']);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Material_Lead');
    }
}
